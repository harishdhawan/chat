(function(){

	var getNode = function(s){
		return document.querySelector(s);
	};

	var status = getNode('.chat-status span');
	var statusDefault = status.textContent;
	var setStatus = function(s){
		status.textContent = s;

		if(status.textContent !== statusDefault){
			//revert after 3 secs
			var delay = setTimeout(function(){
				status.textContent = statusDefault;
				clearInterval(delay);
			}, 2000);
		}
	};


	var textarea = getNode('.chat textarea');
	var chatname = getNode('.chat-name');
	var messages = getNode('.chat-messages');

	try{
		var socket = io.connect('http://127.0.0.1:8080');

	}catch(e){
		//warn user
	}

	if(socket !== undefined){

		//listen output
		socket.on('output', function(data){
			console.log(data);
			var len = data.length;
			if(len){
				for(var x = 0; x < len; x++){
					var message = document.createElement('div');
					message.setAttribute('class', 'chat-message');
					message.textContent = data[x].name + ": " + data[x].message;

					//console.log(message);
					//messages.appendChild(message);
					messages.insertBefore(message, messages.firstChild);
				}
			}
			
		});

		//listen status
		socket.on('status', function(data){
			setStatus(typeof data === 'object' ? data.message : data);

			if(data.clear === true){
				textarea.value = "";
			}
		});

		//listen events
		textarea.addEventListener('keydown', function(event){

			//check for enter key
			if(event.which == 13 && event.shiftKey === false){
				socket.emit('input', {
					name:chatname.value,
					message:this.value
				});

				event.preventDefault();
			}
		});
	}

})();
