
console.log(process.cwd());

var fs = require('fs');

var http = require("http");
var myserver = http.createServer(function(request, response){
	console.log(request.url);

	var resPath = "."; // Relative to current directory
	resPath = resPath + request.url;
	// Special case
	if(request.url === "/"){
		resPath = resPath + "index.html";
	}

	fs.readFile(resPath, function(err, data){
		if (err) {
			response.writeHead(404, {"Content-Type" : "text/plain"});
			response.write("File not found");
			response.end();
			console.log(resPath + "not found.");
		}else{
			if(resPath.match(/.*html/)){
				response.writeHead(200, {"Content-Type" : "text/html"});
			}else if(resPath.match(/.*css/)){
				response.writeHead(200, {"Content-Type" : "text/css"});
			}else{
				response.writeHead(200, {"Content-Type" : "text/plain"});
			}
			
			response.write(data);
			response.end();	
		}
	});
});
myserver.listen(3000);



var mongo = require("mongodb").MongoClient;
	client = require("socket.io").listen(8080).sockets;

mongo.connect('mongodb://127.0.0.1/chat', function(err, db){
	if (err) throw err;

	console.log('Connected to database.');
	var col = db.collection('messages');

	client.on('connection', function(socket){
		//console.log("Someone has connected");

		//send all messages
		col.find().limit(100).sort({_id: 1}).toArray(function(err, res){
			socket.emit('output', res);
		});

		var sendStatus = function(s){
			socket.emit('status', s);
		};

		socket.on('input', function(data){
			var name = data.name;
			var message = data.message;
			var whiteSpacePattern = /^\s*$/;

			if(whiteSpacePattern.test(name) || whiteSpacePattern.test(message)){
				sendStatus('Name and message cannot be empty');
			}else{
				col.insert({name:name, message:message}, function(err){
					console.log('Inserted');

					sendStatus({
						message: "Message sent",
						clear: true
					});

					//emit this message to ALL clients
					client.emit('output', [data]);
				});
			}
		});
	});
});
